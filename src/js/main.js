import data from "./data.json";

document.addEventListener("DOMContentLoaded", function() {
  const Declination = (number, titles) => {
    let cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
  };

  let title = $("#title-map");
  let desc_map = $("#desc-map");
  let players_map = $("#players-map");
  let players_text = $("#players-text");


  const drawText = () => {
    let glowInTexts = document.querySelectorAll(".glowIn");
    glowInTexts.forEach(glowInText => {
      let letters = glowInText.textContent.split("");
      // console.log(letters)
      glowInText.textContent = "";
      letters.forEach((letter, i) => {
        let span = document.createElement("span");
        span.textContent = letter;
        span.style.animationDelay = `${i * 0.02}s`;
        glowInText.append(span);
      });
    });
  };

  drawText();
  $(".map-image svg").click(function(event) {
    let location = $(event.target).attr("id");
    let region = data.filter(item => item.code === location)[0];
    $(title).text(region.title);
    $(desc_map).text(region.desc);
    $(desc_map).text(region.desc);
    $(players_map).text(region.players);
    $(players_text).text(Declination(region.players, ["участник", "участника", "участников"]));
    drawText();
  });

  let x = [".svg"];
  x.forEach(item => {
    $(item).each(function() {
      let $img = $(this);
      let imgClass = $img.attr("class");
      let imgURL = $img.attr("src");
      $.get(imgURL, function(data) {
        let $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
          $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        $img.replaceWith($svg);
      }, "");
    });
  });


  var nav = $("nav");
  var line = $("<div />").addClass("line");

  line.appendTo(nav);

  var active = nav.find(".active");
  var pos = 0;
  var wid = 0;

  if (active.length) {
    pos = active.position().left;
    wid = active.width();
    line.css({
      left: pos,
      width: wid
    });
  }

  function find() {
    nav.find("ul li a").click(function(e) {
      e.preventDefault();
      if (!$(this).parent().hasClass("active") && !nav.hasClass("animate")) {

        nav.addClass("animate");

        var _this = $(this);

        nav.find("ul li").removeClass("active");

        var position = _this.parent().position();
        var width = _this.parent().width();

        if (position.left >= pos) {
          line.animate({
            width: ((position.left - pos) + width)
          }, 300, function() {
            line.animate({
              width: width,
              left: position.left
            }, 150, function() {
              nav.removeClass("animate");
            });
            _this.parent().addClass("active");
          });
        } else {
          line.animate({
            left: position.left,
            width: ((pos - position.left) + wid)
          }, 300, function() {
            line.animate({
              width: width
            }, 150, function() {
              nav.removeClass("animate");
            });
            _this.parent().addClass("active");
          });
        }

        pos = position.left;
        wid = width;
      }
    });
  }

  find();


  $(".map svg").hover(function() {
    console.log("hover");
  });

  $("#sidebarCollapse").on("click", function() {

    $("#sidebar").toggleClass("active");
  });

  $("#dismiss, .overlay").on("click", function() {
    $("#sidebar").removeClass("active");
    $(".overlay").removeClass("active");
    $(".button-mobile-menu").find("#checkbox1").prop("checked", false);
  });

  $("#sidebarCollapse").on("click", function() {
    $("#sidebar").addClass("active");
    $(".overlay").addClass("active");
    $(".collapse.in").toggleClass("in");
    $("a[aria-expanded=true]").attr("aria-expanded", "false");
    $(".button-mobile-menu").find("#checkbox1").prop("checked", true);
  });

  $(".js-news-menu-button").click(function() {

    $(this).toggleClass("news-menu-button--open");
    $(".news-navigation-overlay").toggleClass("news-navigation-overlay--open");
    $(".shift").toggleClass("shift--active");
    let shiftArrowButton = $(".news-navigation-overlay").get(0).getBoundingClientRect().width - $(this).get(0).getBoundingClientRect().width - 9;
    console.log(shiftArrowButton);
    if ($(this).hasClass("news-menu-button--open")) {
      $(".new__image").css("max-height", "484px");
      $(this).css("transform", `rotate(180deg)`);
      $(this).css("left", `${shiftArrowButton}px`);

      $("html, body").animate({ scrollTop: 0 }, "slow");


    } else {
      $(".new__image").css("max-height", "584px");
      $(this).css("transform", "rotate(0deg)");
      $(this).css("left", "8px");
    }


  });

});

