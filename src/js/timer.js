document.addEventListener("DOMContentLoaded", function () {
  const Declination =(number, titles)=> {
    let cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2:cases[(number%10<5)?number%10:5] ];
  }
  if(!document.getElementById('d')) return
  // конечная дата, например 1 июля 2021
  const deadline = new Date(2022, 2, 1);
  // id таймера
  let timerId = null;
  function countdownTimer() {
    const diff = deadline - new Date();
    if (diff <= 0) {
      clearInterval(timerId);
    }
    const days = diff > 0 ? Math.floor(diff / 1000 / 60 / 60 / 24) : 0;
    const hours = diff > 0 ? Math.floor(diff / 1000 / 60 / 60) % 24 : 0;
    const minutes = diff > 0 ? Math.floor(diff / 1000 / 60) % 60 : 0;
    const seconds = diff > 0 ? Math.floor(diff / 1000) % 60 : 0;
    $days.textContent = days < 10 ? '0' + days : days;
    $hours.textContent = hours < 10 ? '0' + hours : hours;
    $minutes.textContent = minutes < 10 ? '0' + minutes : minutes;
    $seconds.textContent = seconds < 10 ? '0' + seconds : seconds;
    $days.dataset.title = Declination(days, ['день', 'дня', 'дней']);
    $hours.dataset.title = Declination(hours, ['час', 'часа', 'часов']);
    $minutes.dataset.title = Declination(minutes, ['минута', 'минуты', 'минут']);
    $seconds.dataset.title = Declination(seconds, ['секунда', 'секунды', 'секунд']);
  }
  // получаем элементы, содержащие компоненты даты
  const $days = document.getElementById('d');
  const $hours = document.getElementById('h');
  const $minutes = document.getElementById('m');
  const $seconds = document.getElementById('s');
  // вызываем функцию countdownTimer
  countdownTimer();
  // вызываем функцию countdownTimer каждую секунду
  timerId = setInterval(countdownTimer, 1000);
})
